FROM marcomaisel/ionic

MAINTAINER engalar <engalar@gmail.com>

ENV PATH $PATH:/opt/gradle/gradle-4.3.1/bin

WORKDIR /builds/engalar/docker-ionic

RUN pwd && \
    ionic start myApp tabs --type=ionic1 —-no-git —-no-link —-cordova && \
    cd myApp && \
    ionic cordova build android && \
    rm -rf /builds/*